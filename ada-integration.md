# Client Credentials - OpenID Connect Flow 

```mermaid
sequenceDiagram
    autoNumber
    participant Quin
    participant ADA

 
    Quin->>ADA: Authenticate with Client ID + Client Secret to /token
    ADA->>ADA: Validate Client ID + Client Secret
 

    ADA->>Quin: Respond with an Access Token


    Quin->>ADA: Consumes API sending the required data + access_token
    ADA->>Quin: Sends a response
```

# API-Key Flow

```mermaid
sequenceDiagram
    autoNumber
    participant Quin
    participant ADA

    Quin->>ADA: Consumes API sending the required data + API Key 
    ADA->>Quin: Sends a response
```